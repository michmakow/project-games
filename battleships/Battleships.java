package Project_Games.battleships;

import Project_Games.sharedClasses.Playable;
import Project_Games.sharedClasses.ProperPrinter;
import Project_Games.sharedClasses.State;

import java.util.Random;
import java.util.Scanner;


public class Battleships implements Playable {
    private Object[][] playerMap = new Object[12][12];
    private Object[][] enemyMap = new Object[12][12];
    private Object[][] playerPicksMap = new Object[12][12];

    private Border border = new Border();
    private Scanner scanner = new Scanner(System.in);

    State state = State.INIT;
    public static final int PRINTER_WIDTH = 40;
    private ProperPrinter pp = new ProperPrinter(PRINTER_WIDTH);

    @Override
    public void start() {

        while (state != State.EXIT) {

            switch (state) {

                case INIT: {

                    pp.delim();
                    pp.print("Welcome in Battleships admiral!");
                    pp.print(" 1 - Start new game");
                    pp.print(" 2 - Difficulty level ");
                    pp.print(" 0 - Exit");
                    pp.delim();

                    String answer = scanner.nextLine();

                    switch (answer) {
                        case ("1"): {
                            state = State.NEW_GAME;
                            break;
                        }
                        case ("2"): {
                            state = State.DIFFICULTY;
                            break;
                        }
                        case ("0"): {
                            state = State.EXIT;
                            break;
                        }
                        default: {
                            System.out.println("Wrong answer");
                            state = State.INIT;
                            break;
                        }
                    }
                }

                case NEW_GAME: {
                    BattleshipsOperator battleshipsOperator = new BattleshipsOperator();
                    battleshipsOperator.enemyShipsPlacement(enemyMap, border);
                    battleshipsOperator.playerShipsPlacement(playerMap, scanner, border);
                    battleshipsOperator.playerPicks(playerPicksMap, border);

                    Random generator = new Random();
                    boolean playerMove = generator.nextBoolean();

                    while (battleshipsOperator.getPlayerShipsCount() > 0 && battleshipsOperator.getEnemyShipsCount() > 0) {

                        while (!playerMove) {

                            if (battleshipsOperator.enemyPicksCommands(playerMap, border)
                                    && battleshipsOperator.getPlayerShipsCount() > 0) {

                                playerMove = false;

                            } else {

                                playerMove = true;

                            }
                        }

                        while (playerMove) {

                            if (battleshipsOperator.playerPicksCommands(scanner, playerPicksMap, border)
                                    && battleshipsOperator.getEnemyShipsCount() > 0) {

                                playerMove = true;

                            } else {

                                playerMove = false;

                            }

                        }
                    }
                    if (battleshipsOperator.getEnemyShipsCount() == 0) {

                        pp.delim();
                        pp.print("U won!!!!");
                        pp.delim();

                    } else if (battleshipsOperator.getPlayerShipsCount() == 0) {

                        pp.delim();
                        pp.print("U lose!!!!");
                        pp.delim();

                    }

                    state = State.INIT;
                    break;
                }

            }
        }
    }


    @Override
    public int getNumber() {
        return 4;
    }

    @Override
    public String getDescription() {
        return "Battleships";
    }
}