package Project_Games.battleships;

import Project_Games.battleships.controllers.DoubleShipController;
import Project_Games.battleships.controllers.QuadraShipController;
import Project_Games.battleships.controllers.SingleShipController;
import Project_Games.battleships.controllers.TripleShipController;
import Project_Games.battleships.models.DoubleShip;
import Project_Games.battleships.views.DoubleShipView;
import Project_Games.battleships.views.QuadraShipView;
import Project_Games.battleships.views.SingleShipView;
import Project_Games.battleships.views.TripleShipView;
import Project_Games.sharedClasses.ProperPrinter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class BattleshipsOperator {
    private static Integer[] tableOfCoordinates = new Integer[3];
    private static final String[] lettersTable = new String[]{
            "x", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};
    private static final String[] numbersTable = new String[]{
            "0", "10", "9", "8", "7", "6", "5", "4", "3", "2", "1"};
    private static final String[] directionTable = new String[]{
            "n", "e", "s", "w"};

    private List<Integer> enemyPicksIList = new ArrayList();
    private List<Integer> enemyPicksJList = new ArrayList();
    private List<Integer> playerPicksIList = new ArrayList();
    private List<Integer> playerPicksJList = new ArrayList();

    private int enemyShipsCount = 20;
    private int playerShipsCount = 20;
    private final int PRINTER_WIDTH = 80;

    private ProperPrinter pp = new ProperPrinter(PRINTER_WIDTH);

    public int getEnemyShipsCount() {
        return enemyShipsCount;
    }

    public int getPlayerShipsCount() {
        return playerShipsCount;
    }

    public Integer[] getTableOfCoordinates() {
        return tableOfCoordinates;
    }

    public boolean enemyPicksCommands(Object[][] playerMap, Border border) {
        Random generator = new Random();
        int coordinatesI = generator.nextInt(9) + 1;
        int coordinatesJ = generator.nextInt(9) + 1;
        String gotAShip = "X ";
        String missed = "O ";

        if (checkIfPickPresent(coordinatesI, coordinatesJ, enemyPicksIList, enemyPicksJList)) {

            enemyPicksCommands(playerMap, border);
            return false;

        }

        pp.print("Enemy on move! Look out!");

        enemyPicksIList.add(coordinatesI);
        enemyPicksJList.add(coordinatesJ);

        if ((QuadraShipController.checkIfPlayerQuadraPresent(coordinatesI, coordinatesJ))) {

            playerMap[coordinatesI][coordinatesJ] = gotAShip;
            playerShipsCount--;
            border.mapDisplay(playerMap);
            QuadraShipView.enemyHitFourMasted();
            return true;

        } else if (TripleShipController.checkIfPlayerTriplePresent(coordinatesI, coordinatesJ)) {

            playerMap[coordinatesI][coordinatesJ] = gotAShip;
            playerShipsCount--;
            border.mapDisplay(playerMap);
            TripleShipView.enemyHitThreeMasted();
            return true;

        } else if (DoubleShipController.checkIfPlayerDoublePresent(coordinatesI, coordinatesJ)) {

            playerMap[coordinatesI][coordinatesJ] = gotAShip;
            playerShipsCount--;
            border.mapDisplay(playerMap);
            DoubleShipView.enemyHitTwoMasted();
            return true;

        } else if (SingleShipController.checkIfPlayerSinglePresent(coordinatesI, coordinatesJ)) {

            playerMap[coordinatesI][coordinatesJ] = gotAShip;
            playerShipsCount--;
            border.mapDisplay(playerMap);
            SingleShipView.enemyHitOneMasted();
            return true;

        } else {

            playerMap[coordinatesI][coordinatesJ] = missed;
            border.mapDisplay(playerMap);
            pp.print("Enemy missed!!");
            return false;

        }
    }

    public boolean playerPicksCommands(Scanner scanner, Object[][] playerPicksMap, Border border) {
        String youGotAShip = "X ";
        String youMissed = "O ";

        pp.print("Your move admiral! Set your coordinates to attack");

        String coordinates = scanner.nextLine();
        String[] stringTable = coordinates.toLowerCase().split(" ");

        while (stringTable.length > 2 || stringTable.length < 2) {
            pp.print("Wrong input. Please try again.");
            coordinates = scanner.nextLine();
            stringTable = coordinates.toLowerCase().trim().split(" ");
        }

        checkIfLetterPresent(stringTable[0]);
        checkIfNumberPresent(stringTable[1]);

        if (checkIfPickPresent(tableOfCoordinates[0], tableOfCoordinates[1], playerPicksIList, playerPicksJList)) {

            pp.print("Admiral, you want to hit the same spot! Try again");
            playerPicksCommands(scanner, playerPicksMap, border);
            return false;

        }


        playerPicksIList.add(tableOfCoordinates[0]);
        playerPicksJList.add(tableOfCoordinates[1]);

        if ((QuadraShipController.checkIfEnemyQuadraPresent(tableOfCoordinates[1], tableOfCoordinates[0]))) {
            QuadraShipView.youHitFourMasted();
            playerPicksMap[tableOfCoordinates[1]][tableOfCoordinates[0]] = youGotAShip;
            enemyShipsCount--;
            border.mapDisplay(playerPicksMap);
            return true;

        } else if (TripleShipController.checkIfEnemyTriplePresent(tableOfCoordinates[1], tableOfCoordinates[0])) {

            TripleShipView.youHitThreeMasted();
            playerPicksMap[tableOfCoordinates[1]][tableOfCoordinates[0]] = youGotAShip;
            enemyShipsCount--;
            border.mapDisplay(playerPicksMap);
            return true;

        } else if (DoubleShipController.checkIfEnemyDoublePresent(tableOfCoordinates[1], tableOfCoordinates[0])) {

            DoubleShipView.youHitTwoMasted();
            playerPicksMap[tableOfCoordinates[1]][tableOfCoordinates[0]] = youGotAShip;
            enemyShipsCount--;
            border.mapDisplay(playerPicksMap);
            return true;

        } else if (SingleShipController.checkIfEnemySinglePresent(tableOfCoordinates[1], tableOfCoordinates[0])) {

            SingleShipView.youHitOneMasted();
            playerPicksMap[tableOfCoordinates[1]][tableOfCoordinates[0]] = youGotAShip;
            enemyShipsCount--;
            border.mapDisplay(playerPicksMap);
            return true;

        } else {

            playerPicksMap[tableOfCoordinates[1]][tableOfCoordinates[0]] = youMissed;
            border.mapDisplay(playerPicksMap);
            pp.print("You missed!");
            return false;

        }
    }

    public void playerPicks(Object[][] playerPicksMap, Border border) {
        border.mapFormatter(playerPicksMap);
    }

    public void enemyShipsPlacement(Object[][] enemyMap, Border border) {
        border.mapFormatter(enemyMap);

        QuadraShipController quadraShipController = new QuadraShipController();
        TripleShipController tripleShipController = new TripleShipController();
        DoubleShipController doubleShipController = new DoubleShipController();
        SingleShipController singleShipController = new SingleShipController();

        quadraShipController.enemyQuadraShipPlacementAndDraw(enemyMap);

        for (int i = 0; i < 2; i++) {
            tripleShipController.enemyTripleShipPlacementAndDraw(enemyMap);
        }

        for (int i = 0; i < 3; i++) {
            doubleShipController.enemyDoubleShipPlacementAndDraw(enemyMap);
        }

        for (int i = 0; i < 4; i++) {
            singleShipController.enemySingleShipPlacementAndDraw(enemyMap);
        }
    }

    public void playerShipsPlacement(Object[][] playerMap, Scanner scanner, Border border) {
        border.mapFormatter(playerMap);
        border.mapDisplay(playerMap);

        QuadraShipController quadraShipController = new QuadraShipController();
        TripleShipController tripleShipController = new TripleShipController();
        DoubleShipController doubleShipController = new DoubleShipController();
        SingleShipController singleShipController = new SingleShipController();

        QuadraShipView.placeYourFourMastedShip();
        quadraShipController.playerQuadraShipPlacementAndDraw(playerMap, scanner);
        border.mapDisplay(playerMap);

        for (int i = 0; i < 2; i++) {
            TripleShipView.placeYourThreeMastedShip();
            tripleShipController.playerTripleShipPlacementAndDraw(playerMap, scanner);
            border.mapDisplay(playerMap);
        }

        for (int i = 0; i < 3; i++) {
            DoubleShipView.placeYourTwoMastedShip();
            doubleShipController.playerDoubleShipPlacementAndDraw(playerMap, scanner);
            border.mapDisplay(playerMap);
        }

        for (int i = 0; i < 4; i++) {
            SingleShipView.placeYourOneMastedShip();
            singleShipController.playerSingleShipPlacementAndDraw(playerMap, scanner);
            border.mapDisplay(playerMap);
        }
    }

    public void shipPlacementCommand(Scanner scanner) {

        pp.print("Set your coordinates: ex. G 10 N/E/S/W(N - north, E - east etc.) ");
        String coordinates = scanner.nextLine();

        String[] stringTable = coordinates.toLowerCase().split(" ");

        while (stringTable.length > 3 || stringTable.length < 3) {
            System.out.println("Wrong input. Please try again.");
            pp.print("Set your coordinates: ex. G 10 N/E/S/W(N - north, E - east etc.) ");
            coordinates = scanner.nextLine();
            stringTable = coordinates.toLowerCase().split(" ");
        }

        if (!checkIfLetterPresent(stringTable[0]) || !checkIfNumberPresent(stringTable[1])
                || !checkIfDirectionPresent(stringTable[2])) {
            System.out.println("Wrong input. Please try again.");
            shipPlacementCommand(scanner);
        }
    }

    private static boolean checkIfLetterPresent(String string) {
        for (int i = 0; i < lettersTable.length; i++) {
            if (lettersTable[i].contains(string)) {
                tableOfCoordinates[0] = i;
                return true;
            }
        }
        return false;
    }

    private static boolean checkIfNumberPresent(String string) {
        for (int i = 0; i < numbersTable.length; i++) {
            if (numbersTable[i].equals(string)) {
                tableOfCoordinates[1] = i;
                return true;
            }
        }
        return false;
    }

    private static boolean checkIfDirectionPresent(String string) {
        for (int i = 0; i < directionTable.length; i++) {
            if (directionTable[i].contains(string)) {
                tableOfCoordinates[2] = i;
                return true;
            }
        }
        return false;
    }

    private static boolean checkIfPickPresent(Integer numberI, Integer numberJ, List picksIList, List picksJList) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < picksIList.size(); i++) {
            if (picksIList.get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (picksJList.get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }
}
