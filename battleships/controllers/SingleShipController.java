package Project_Games.battleships.controllers;

import Project_Games.battleships.BattleshipsOperator;
import Project_Games.battleships.models.SingleShip;
import Project_Games.battleships.views.SingleShipView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class SingleShipController {

    public Object[][] enemySingleShipPlacementAndDraw(Object[][] enemyMap) {

        Random generator = new Random();
        SingleShip singleShip = new SingleShip();

        int shipI = generator.nextInt(9) + 1;
        int shipJ = generator.nextInt(9) + 1;

        while (QuadraShipController.checkIfEnemyQuadraBordersPresent(shipI, shipJ)
                || TripleShipController.checkIfEnemyTripleBordersPresent(shipI, shipJ)
                || DoubleShipController.checkIfEnemyDoubleBordersPresent(shipI, shipJ)
                || SingleShipController.checkIfEnemySingleBordersPresent(shipI, shipJ)) {

            shipI = generator.nextInt(9) + 1;
            shipJ = generator.nextInt(9) + 1;
        }

        enemyMap[shipI][shipJ] = singleShip;
        SingleShip.addEnemySingleShipIList(shipI);
        SingleShip.addEnemySingleShipJList(shipJ);

        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                SingleShip.addEnemySingleBorderShipIList(shipI + i);
                SingleShip.addEnemySingleBorderShipJList(shipJ + j);
            }
        }

        return enemyMap;
    }

    public Object[][] playerSingleShipPlacementAndDraw(Object[][] playerMap, Scanner scanner) {

        BattleshipsOperator battleshipsOperator = new BattleshipsOperator();

        battleshipsOperator.shipPlacementCommand(scanner);

        int playerShipJ = 0;
        int playerShipI = 0;

        try {

            playerShipJ = battleshipsOperator.getTableOfCoordinates()[0];
            playerShipI = battleshipsOperator.getTableOfCoordinates()[1];

        } catch (NullPointerException e) {

            SingleShipView.wrongInput();
            playerSingleShipPlacementAndDraw(playerMap, scanner);

        }

        SingleShip singleShip = new SingleShip();

        if (checkIfPlayerSingleBordersPresent(playerShipI, playerShipJ)
                || DoubleShipController.checkIfPlayerDoubleBordersPresent(playerShipI, playerShipJ)
                || TripleShipController.checkIfPlayerTripleBordersPresent(playerShipI, playerShipJ)
                || QuadraShipController.checkIfPlayerQuadraBordersPresent(playerShipI, playerShipJ)) {

            SingleShipView.chooseDiffDirOrCoordi();
            playerSingleShipPlacementAndDraw(playerMap, scanner);

        } else {

            playerMap[playerShipI][playerShipJ] = singleShip;

            SingleShip.addPlayerSingleShipIList(playerShipI);
            SingleShip.addPlayerSingleShipJList(playerShipJ);

            for (int i = -1; i < 2; i++) {
                for (int j = -1; j < 2; j++) {
                    SingleShip.addPlayerSingleBorderShipIList(playerShipI + i);
                    SingleShip.addPlayerSingleBorderShipJList(playerShipJ + j);
                }
            }
        }

        return playerMap;
    }

    public static boolean checkIfEnemySinglePresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < SingleShip.getEnemySingleShipIList().size(); i++) {
            if (SingleShip.getEnemySingleShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (SingleShip.getEnemySingleShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfPlayerSinglePresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < SingleShip.getPlayerSingleShipIList().size(); i++) {
            if (SingleShip.getPlayerSingleShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (SingleShip.getPlayerSingleShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfEnemySingleBordersPresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < SingleShip.getEnemySingleBorderShipIList().size(); i++) {
            if (SingleShip.getEnemySingleBorderShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }

        for (Integer list : indexList) {
            if (SingleShip.getEnemySingleBorderShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfPlayerSingleBordersPresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < SingleShip.getPlayerSingleBorderShipIList().size(); i++) {
            if (SingleShip.getPlayerSingleBorderShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }

        for (Integer list : indexList) {
            if (SingleShip.getPlayerSingleBorderShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }
}
