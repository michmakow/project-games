package Project_Games.battleships.controllers;

import Project_Games.battleships.BattleshipsOperator;
import Project_Games.battleships.models.DoubleShip;
import Project_Games.battleships.views.DoubleShipView;
import Project_Games.battleships.views.TripleShipView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class DoubleShipController {

    public Object[][] enemyDoubleShipPlacementAndDraw(Object[][] enemyMap) {

        Random generator = new Random();
        DoubleShip doubleShip = new DoubleShip();

        int shipI = generator.nextInt(9) + 1;
        int shipJ = generator.nextInt(9) + 1;
        int shipDrawDirection = generator.nextInt(3);

        if (enemyDoubleShipDirectionChecker(enemyMap, doubleShip, shipI, shipJ, shipDrawDirection)) {

            enemyDoubleShipPlacementAndDraw(enemyMap);

        } else {

            enemyMap[shipI][shipJ] = doubleShip;

            doubleShipDraw(enemyMap, doubleShip, shipI, shipJ, shipDrawDirection, DoubleShip.getEnemyDoubleShipIList(),
                    DoubleShip.getEnemyDoubleShipJList(), DoubleShip.getEnemyDoubleBorderShipIList(),
                    DoubleShip.getEnemyDoubleBorderShipJList());
        }

        return enemyMap;

    }

    public Object[][] playerDoubleShipPlacementAndDraw(Object[][] playerMap, Scanner scanner) {

        BattleshipsOperator battleshipsOperator = new BattleshipsOperator();

        battleshipsOperator.shipPlacementCommand(scanner);

        int playerShipJ = 0;
        int playerShipI = 0;
        int playerDrawDirection = 4;

        try {

            playerShipJ = battleshipsOperator.getTableOfCoordinates()[0];
            playerShipI = battleshipsOperator.getTableOfCoordinates()[1];
            playerDrawDirection = battleshipsOperator.getTableOfCoordinates()[2];

        } catch (NullPointerException e) {

            TripleShipView.wrongInput();
            playerDoubleShipPlacementAndDraw(playerMap, scanner);

        }

        DoubleShip doubleShip = new DoubleShip();
        doubleShip.setDrawDirection(playerDrawDirection);

        if (playerDoubleShipDirectionChecker(playerMap, doubleShip, playerShipI, playerShipJ, playerDrawDirection)) {

            DoubleShipView.chooseDiffDirOrCoordi();
            playerDoubleShipPlacementAndDraw(playerMap, scanner);

        } else {

            doubleShipDraw(playerMap, doubleShip, playerShipI, playerShipJ, playerDrawDirection, DoubleShip.getPlayerDoubleShipIList(),
                    DoubleShip.getPlayerDoubleShipJList(), DoubleShip.getPlayerDoubleBorderShipIList(),
                    DoubleShip.getPlayerDoubleBorderShipJList());

        }

        return playerMap;
    }

    private boolean enemyDoubleShipDirectionChecker(Object[][] map, DoubleShip doubleShip, Integer shipI, Integer shipJ,
                                                    Integer direction) {
        int doubleShipCount = 0;

        while (doubleShipCount < doubleShip.getFlagCount()) {

            if (direction == 0) {

                if (map[shipI - doubleShipCount][shipJ] == map[0][shipJ]
                        || QuadraShipController.checkIfEnemyQuadraBordersPresent(shipI - doubleShipCount, shipJ)
                        || TripleShipController.checkIfEnemyTripleBordersPresent(shipI - doubleShipCount, shipJ)
                        || DoubleShipController.checkIfEnemyDoubleBordersPresent(shipI - doubleShipCount, shipJ)) {

                    return true;

                } else {

                    doubleShipCount++;

                }

            } else if (direction == 1) {

                if (map[shipI][shipJ + doubleShipCount] == map[shipI][11]
                        || QuadraShipController.checkIfEnemyQuadraBordersPresent(shipI, shipJ + doubleShipCount)
                        || TripleShipController.checkIfEnemyTripleBordersPresent(shipI, shipJ + doubleShipCount)
                        || DoubleShipController.checkIfEnemyDoubleBordersPresent(shipI, shipJ + doubleShipCount)) {

                    return true;

                } else {

                    doubleShipCount++;

                }

            } else if (direction == 2) {

                if (map[shipI + doubleShipCount][shipJ] == map[11][shipJ]
                        || QuadraShipController.checkIfEnemyQuadraBordersPresent(shipI + doubleShipCount, shipJ)
                        || TripleShipController.checkIfEnemyTripleBordersPresent(shipI + doubleShipCount, shipJ)
                        || DoubleShipController.checkIfEnemyDoubleBordersPresent(shipI + doubleShipCount, shipJ)) {

                    return true;

                } else {

                    doubleShipCount++;

                }

            } else if (direction == 3) {

                if (map[shipI][shipJ - doubleShipCount] == map[shipI][0]
                        || QuadraShipController.checkIfEnemyQuadraBordersPresent(shipI, shipJ - doubleShipCount)
                        || TripleShipController.checkIfEnemyTripleBordersPresent(shipI, shipJ - doubleShipCount)
                        || DoubleShipController.checkIfEnemyDoubleBordersPresent(shipI, shipJ - doubleShipCount)) {

                    return true;

                } else {

                    doubleShipCount++;

                }
            }
        }
        return false;
    }

    private boolean playerDoubleShipDirectionChecker(Object[][] map, DoubleShip doubleShip, Integer shipI, Integer shipJ,
                                                     Integer direction) {
        int doubleShipCount = 0;

        while (doubleShipCount < doubleShip.getFlagCount()) {

            if (direction == 0) {

                if (map[shipI - doubleShipCount][shipJ] == map[0][shipJ]
                        || QuadraShipController.checkIfPlayerQuadraBordersPresent(shipI - doubleShipCount, shipJ)
                        || TripleShipController.checkIfPlayerTripleBordersPresent(shipI - doubleShipCount, shipJ)
                        || DoubleShipController.checkIfPlayerDoubleBordersPresent(shipI - doubleShipCount, shipJ)) {

                    return true;

                } else {

                    doubleShipCount++;

                }

            } else if (direction == 1) {

                if (map[shipI][shipJ + doubleShipCount] == map[shipI][11]
                        || QuadraShipController.checkIfPlayerQuadraBordersPresent(shipI, shipJ + doubleShipCount)
                        || TripleShipController.checkIfPlayerTripleBordersPresent(shipI, shipJ + doubleShipCount)
                        || DoubleShipController.checkIfPlayerDoubleBordersPresent(shipI, shipJ + doubleShipCount)) {

                    return true;

                } else {

                    doubleShipCount++;

                }

            } else if (direction == 2) {

                if (map[shipI + doubleShipCount][shipJ] == map[11][shipJ]
                        || QuadraShipController.checkIfPlayerQuadraBordersPresent(shipI + doubleShipCount, shipJ)
                        || TripleShipController.checkIfPlayerTripleBordersPresent(shipI + doubleShipCount, shipJ)
                        || DoubleShipController.checkIfPlayerDoubleBordersPresent(shipI + doubleShipCount, shipJ)) {

                    return true;

                } else {

                    doubleShipCount++;

                }

            } else if (direction == 3) {

                if (map[shipI][shipJ - doubleShipCount] == map[shipI][0]
                        || QuadraShipController.checkIfPlayerQuadraBordersPresent(shipI, shipJ - doubleShipCount)
                        || TripleShipController.checkIfPlayerTripleBordersPresent(shipI, shipJ - doubleShipCount)
                        || DoubleShipController.checkIfPlayerDoubleBordersPresent(shipI, shipJ - doubleShipCount)) {

                    return true;

                } else {

                    doubleShipCount++;

                }
            }
        }
        return false;
    }

    private void doubleShipDraw(Object[][] map, DoubleShip doubleShip, Integer shipI, Integer shipJ, Integer direction,
                                List doubleShipIList, List doubleShipJList, List doubleBorderShipIList, List doubleBorderShipJList) {

        int doubleShipCount = 0;

        //DoubleShip and doubleship border draw
        while (doubleShipCount < doubleShip.getFlagCount()) {

            if (direction == 0) {

                map[shipI - doubleShipCount][shipJ] = doubleShip;
                doubleShipIList.add(shipI - doubleShipCount);
                doubleShipJList.add(shipJ);

                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        doubleBorderShipIList.add(shipI - doubleShipCount + i);
                        doubleBorderShipJList.add(shipJ + j);
                    }
                }
                doubleShipCount++;

            } else if (direction == 1) {

                map[shipI][shipJ + doubleShipCount] = doubleShip;
                doubleShipIList.add(shipI);
                doubleShipJList.add(shipJ + doubleShipCount);

                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        doubleBorderShipIList.add(shipI + j);
                        doubleBorderShipJList.add(shipJ + doubleShipCount + i);
                    }
                }
                doubleShipCount++;

            } else if (direction == 2) {

                map[shipI + doubleShipCount][shipJ] = doubleShip;
                doubleShipIList.add(shipI + doubleShipCount);
                doubleShipJList.add(shipJ);

                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        doubleBorderShipIList.add(shipI + doubleShipCount + i);
                        doubleBorderShipJList.add(shipJ + j);
                    }
                }
                doubleShipCount++;

            } else if (direction == 3) {

                map[shipI][shipJ - doubleShipCount] = doubleShip;
                doubleShipIList.add(shipI);
                doubleShipJList.add(shipJ - doubleShipCount);

                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        doubleBorderShipIList.add(shipI + j);
                        doubleBorderShipJList.add(shipJ - doubleShipCount + i);
                    }
                }
                doubleShipCount++;
            }
        }
    }

    public static boolean checkIfEnemyDoublePresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < DoubleShip.getEnemyDoubleShipIList().size(); i++) {
            if (DoubleShip.getEnemyDoubleShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (DoubleShip.getEnemyDoubleShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfPlayerDoublePresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < DoubleShip.getPlayerDoubleShipIList().size(); i++) {
            if (DoubleShip.getPlayerDoubleShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (DoubleShip.getPlayerDoubleShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfEnemyDoubleBordersPresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < DoubleShip.getEnemyDoubleBorderShipIList().size(); i++) {
            if (DoubleShip.getEnemyDoubleBorderShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (DoubleShip.getEnemyDoubleBorderShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfPlayerDoubleBordersPresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < DoubleShip.getPlayerDoubleBorderShipIList().size(); i++) {
            if (DoubleShip.getPlayerDoubleBorderShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (DoubleShip.getPlayerDoubleBorderShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }

}
