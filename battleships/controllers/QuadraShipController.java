package Project_Games.battleships.controllers;

import Project_Games.battleships.BattleshipsOperator;
import Project_Games.battleships.models.QuadraShip;
import Project_Games.battleships.views.QuadraShipView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class QuadraShipController {

    public Object[][] enemyQuadraShipPlacementAndDraw(Object[][] enemyMap) {

        Random generator = new Random();
        QuadraShip quadraShip = new QuadraShip();

        int shipI = generator.nextInt(9) + 1;
        int shipJ = generator.nextInt(9) + 1;
        int shipDrawDirection = generator.nextInt(3);

        if (quadraShipDirectionChecker(enemyMap, quadraShip, shipI, shipJ, shipDrawDirection)) {

            enemyQuadraShipPlacementAndDraw(enemyMap);

        } else {

            enemyMap[shipI][shipJ] = quadraShip;

            quadraShipDraw(enemyMap, quadraShip, shipI, shipJ, shipDrawDirection, QuadraShip.getEnemyQuadraShipIList(),
                    QuadraShip.getEnemyQuadraShipJList(), QuadraShip.getEnemyQuadraBorderShipIList(),
                    QuadraShip.getEnemyQuadraBorderShipJList());

        }

        return enemyMap;

    }

    public Object[][] playerQuadraShipPlacementAndDraw(Object[][] playerMap, Scanner scanner) {

        BattleshipsOperator battleshipsOperator = new BattleshipsOperator();

        battleshipsOperator.shipPlacementCommand(scanner);

        int playerShipJ = 0;
        int playerShipI = 0;
        int playerDrawDirection = 4;

        try {

            playerShipJ = battleshipsOperator.getTableOfCoordinates()[0];
            playerShipI = battleshipsOperator.getTableOfCoordinates()[1];
            playerDrawDirection = battleshipsOperator.getTableOfCoordinates()[2];

        } catch (NullPointerException e) {

            QuadraShipView.wrongInput();
            playerQuadraShipPlacementAndDraw(playerMap, scanner);

        }

        QuadraShip quadraShip = new QuadraShip();
        quadraShip.setDrawDirection(playerDrawDirection);

        if (quadraShipDirectionChecker(playerMap, quadraShip, playerShipI, playerShipJ, playerDrawDirection)) {

            QuadraShipView.chooseDiffDirOrCoordi();
            playerQuadraShipPlacementAndDraw(playerMap, scanner);

        } else {

            quadraShipDraw(playerMap, quadraShip, playerShipI, playerShipJ, playerDrawDirection, QuadraShip.getPlayerQuadraShipIList(),
                    QuadraShip.getPlayerQuadraShipJList(), QuadraShip.getPlayerQuadraBorderShipIList(),
                    QuadraShip.getPlayerQuadraBorderShipJList());

        }

        return playerMap;
    }

    public boolean quadraShipDirectionChecker(Object[][] map, QuadraShip quadraShip, Integer shipI, Integer shipJ, Integer direction) {
        int quadraShipCount = 1;

        while (quadraShipCount < quadraShip.getFlagCount()) {

            if (direction == 0) {

                if (map[shipI - quadraShipCount][shipJ] == map[0][shipJ]) {

                    return true;

                } else {

                    quadraShipCount++;

                }

            } else if (direction == 1) {

                if (map[shipI][shipJ + quadraShipCount] == map[shipI][11]) {

                    return true;

                } else {

                    quadraShipCount++;

                }

            } else if (direction == 2) {

                if (map[shipI + quadraShipCount][shipJ] == map[11][shipJ]) {

                    return true;

                } else {

                    quadraShipCount++;

                }

            } else if (direction == 3) {

                if (map[shipI][shipJ - quadraShipCount] == map[shipI][0]) {

                    return true;

                } else {

                    quadraShipCount++;

                }
            }
        }
        return false;
    }

    private void quadraShipDraw(Object[][] map, QuadraShip quadraShip, Integer shipI, Integer shipJ, Integer direction,
                                List quadraShipIList, List quadraShipJList, List quadraBorderShipIList, List quadraBorderShipJList) {

        int quadraShipCount = 0;

        //QuadraShip and Quadraship border draw
        while (quadraShipCount < quadraShip.getFlagCount()) {

            if (direction == 0) {

                map[shipI - quadraShipCount][shipJ] = quadraShip;
                quadraShipIList.add(shipI - quadraShipCount);
                quadraShipJList.add(shipJ);

                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        quadraBorderShipIList.add(shipI - quadraShipCount + i);
                        quadraBorderShipJList.add(shipJ + j);
                    }
                }
                quadraShipCount++;

            } else if (direction == 1) {

                map[shipI][shipJ + quadraShipCount] = quadraShip;
                quadraShipIList.add(shipI);
                quadraShipJList.add(shipJ + quadraShipCount);

                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        quadraBorderShipIList.add(shipI + j);
                        quadraBorderShipJList.add(shipJ + quadraShipCount + i);
                    }
                }
                quadraShipCount++;

            } else if (direction == 2) {

                map[shipI + quadraShipCount][shipJ] = quadraShip;
                quadraShipIList.add(shipI + quadraShipCount);
                quadraShipJList.add(shipJ);

                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        quadraBorderShipIList.add(shipI + quadraShipCount + i);
                        quadraBorderShipJList.add(shipJ + j);
                    }
                }
                quadraShipCount++;

            } else if (direction == 3) {

                map[shipI][shipJ - quadraShipCount] = quadraShip;
                quadraShipIList.add(shipI);
                quadraShipJList.add(shipJ - quadraShipCount);

                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        quadraBorderShipIList.add(shipI + j);
                        quadraBorderShipJList.add(shipJ - quadraShipCount + i);
                    }
                }
                quadraShipCount++;
            }
        }

    }

    public static boolean checkIfEnemyQuadraPresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < QuadraShip.getEnemyQuadraShipIList().size(); i++) {
            if (QuadraShip.getEnemyQuadraShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (QuadraShip.getEnemyQuadraShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfPlayerQuadraPresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < QuadraShip.getPlayerQuadraShipIList().size(); i++) {
            if (QuadraShip.getPlayerQuadraShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (QuadraShip.getPlayerQuadraShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfEnemyQuadraBordersPresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < QuadraShip.getEnemyQuadraBorderShipIList().size(); i++) {
            if (QuadraShip.getEnemyQuadraBorderShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (QuadraShip.getEnemyQuadraBorderShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfPlayerQuadraBordersPresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < QuadraShip.getPlayerQuadraBorderShipIList().size(); i++) {
            if (QuadraShip.getPlayerQuadraBorderShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (QuadraShip.getPlayerQuadraBorderShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }


}
