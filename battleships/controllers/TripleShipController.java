package Project_Games.battleships.controllers;

import Project_Games.battleships.BattleshipsOperator;
import Project_Games.battleships.models.TripleShip;
import Project_Games.battleships.views.TripleShipView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class TripleShipController {

    public Object[][] enemyTripleShipPlacementAndDraw(Object[][] enemyMap) {

        Random generator = new Random();
        TripleShip tripleShip = new TripleShip();

        int shipI = generator.nextInt(9) + 1;
        int shipJ = generator.nextInt(9) + 1;
        int shipDrawDirection = generator.nextInt(3);

        if (enemyTripleShipDirectionChecker(enemyMap, tripleShip, shipI, shipJ, shipDrawDirection)) {

            enemyTripleShipPlacementAndDraw(enemyMap);

        } else {

            enemyMap[shipI][shipJ] = tripleShip;

            tripleShipDraw(enemyMap, tripleShip, shipI, shipJ, shipDrawDirection, tripleShip.getEnemyTripleShipIList(),
                    tripleShip.getEnemyTripleShipJList(), tripleShip.getEnemyTripleBorderShipIList(),
                    tripleShip.getEnemyTripleBorderShipJList());
        }

        return enemyMap;

    }

    public Object[][] playerTripleShipPlacementAndDraw(Object[][] playerMap, Scanner scanner) {

        BattleshipsOperator battleshipsOperator = new BattleshipsOperator();

        battleshipsOperator.shipPlacementCommand(scanner);

        int playerShipJ = 0;
        int playerShipI = 0;
        int playerDrawDirection = 4;

        try {

            playerShipJ = battleshipsOperator.getTableOfCoordinates()[0];
            playerShipI = battleshipsOperator.getTableOfCoordinates()[1];
            playerDrawDirection = battleshipsOperator.getTableOfCoordinates()[2];

        } catch (NullPointerException e) {

            TripleShipView.wrongInput();
            playerTripleShipPlacementAndDraw(playerMap, scanner);

        }

        TripleShip tripleShip = new TripleShip();
        tripleShip.setDrawDirection(playerDrawDirection);

        if (playerTripleShipDirectionChecker(playerMap, tripleShip, playerShipI, playerShipJ, playerDrawDirection)) {

            TripleShipView.chooseDiffDirOrCoordi();
            playerTripleShipPlacementAndDraw(playerMap, scanner);

        } else {

            tripleShipDraw(playerMap, tripleShip, playerShipI, playerShipJ, playerDrawDirection, tripleShip.getPlayerTripleShipIList(),
                    tripleShip.getPlayerTripleShipJList(), tripleShip.getPlayerTripleBorderShipIList(),
                    tripleShip.getPlayerTripleBorderShipJList());

        }

        return playerMap;
    }

    private boolean enemyTripleShipDirectionChecker(Object[][] map, TripleShip tripleShip, Integer shipI, Integer shipJ,
                                                    Integer direction) {
        int tripleShipCount = 0;

        while (tripleShipCount < tripleShip.getFlagCount()) {

            if (direction == 0) {

                if (map[shipI - tripleShipCount][shipJ] == map[0][shipJ]
                        || QuadraShipController.checkIfEnemyQuadraBordersPresent(shipI - tripleShipCount, shipJ)
                        || TripleShipController.checkIfEnemyTripleBordersPresent(shipI - tripleShipCount, shipJ)) {

                    return true;

                } else {

                    tripleShipCount++;

                }

            } else if (direction == 1) {

                if (map[shipI][shipJ + tripleShipCount] == map[shipI][11]
                        || QuadraShipController.checkIfEnemyQuadraBordersPresent(shipI, shipJ + tripleShipCount)
                        || TripleShipController.checkIfEnemyTripleBordersPresent(shipI, shipJ + tripleShipCount)) {

                    return true;

                } else {

                    tripleShipCount++;

                }

            } else if (direction == 2) {

                if (map[shipI + tripleShipCount][shipJ] == map[11][shipJ]
                        || QuadraShipController.checkIfEnemyQuadraBordersPresent(shipI + tripleShipCount, shipJ)
                        || TripleShipController.checkIfEnemyTripleBordersPresent(shipI + tripleShipCount, shipJ)) {

                    return true;

                } else {

                    tripleShipCount++;

                }

            } else if (direction == 3) {

                if (map[shipI][shipJ - tripleShipCount] == map[shipI][0]
                        || QuadraShipController.checkIfEnemyQuadraBordersPresent(shipI, shipJ - tripleShipCount)
                        || TripleShipController.checkIfEnemyTripleBordersPresent(shipI, shipJ - tripleShipCount)) {

                    return true;

                } else {

                    tripleShipCount++;

                }
            }
        }
        return false;
    }

    private boolean playerTripleShipDirectionChecker(Object[][] map, TripleShip tripleShip, Integer shipI, Integer shipJ,
                                                     Integer direction) {
        int tripleShipCount = 0;

        while (tripleShipCount < tripleShip.getFlagCount()) {

            if (direction == 0) {

                if (map[shipI - tripleShipCount][shipJ] == map[0][shipJ]
                        || QuadraShipController.checkIfPlayerQuadraBordersPresent(shipI - tripleShipCount, shipJ)
                        || TripleShipController.checkIfPlayerTripleBordersPresent(shipI - tripleShipCount, shipJ)) {

                    return true;

                } else {

                    tripleShipCount++;

                }

            } else if (direction == 1) {

                if (map[shipI][shipJ + tripleShipCount] == map[shipI][11]
                        || QuadraShipController.checkIfPlayerQuadraBordersPresent(shipI, shipJ + tripleShipCount)
                        || TripleShipController.checkIfPlayerTripleBordersPresent(shipI, shipJ + tripleShipCount)) {

                    return true;

                } else {

                    tripleShipCount++;

                }

            } else if (direction == 2) {

                if (map[shipI + tripleShipCount][shipJ] == map[11][shipJ]
                        || QuadraShipController.checkIfPlayerQuadraBordersPresent(shipI + tripleShipCount, shipJ)
                        || TripleShipController.checkIfPlayerTripleBordersPresent(shipI + tripleShipCount, shipJ)) {

                    return true;

                } else {

                    tripleShipCount++;

                }

            } else if (direction == 3) {

                if (map[shipI][shipJ - tripleShipCount] == map[shipI][0]
                        || QuadraShipController.checkIfPlayerQuadraBordersPresent(shipI, shipJ - tripleShipCount)
                        || TripleShipController.checkIfPlayerTripleBordersPresent(shipI, shipJ - tripleShipCount)) {

                    return true;

                } else {

                    tripleShipCount++;

                }
            }
        }
        return false;
    }

    private void tripleShipDraw(Object[][] map, TripleShip tripleShip, Integer shipI, Integer shipJ, Integer direction,
                                List tripleShipIList, List tripleShipJList, List tripleBorderShipIList, List tripleBorderShipJList) {

        int tripleShipCount = 0;

        //TripleShip and tripleship border draw
        while (tripleShipCount < tripleShip.getFlagCount()) {

            if (direction == 0) {

                map[shipI - tripleShipCount][shipJ] = tripleShip;
                tripleShipIList.add(shipI - tripleShipCount);
                tripleShipJList.add(shipJ);

                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        tripleBorderShipIList.add(shipI - tripleShipCount + i);
                        tripleBorderShipJList.add(shipJ + j);
                    }
                }
                tripleShipCount++;

            } else if (direction == 1) {

                map[shipI][shipJ + tripleShipCount] = tripleShip;
                tripleShipIList.add(shipI);
                tripleShipJList.add(shipJ + tripleShipCount);

                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        tripleBorderShipIList.add(shipI + j);
                        tripleBorderShipJList.add(shipJ + tripleShipCount + i);
                    }
                }
                tripleShipCount++;

            } else if (direction == 2) {

                map[shipI + tripleShipCount][shipJ] = tripleShip;
                tripleShipIList.add(shipI + tripleShipCount);
                tripleShipJList.add(shipJ);

                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        tripleBorderShipIList.add(shipI + tripleShipCount + i);
                        tripleBorderShipJList.add(shipJ + j);
                    }
                }
                tripleShipCount++;

            } else if (direction == 3) {

                map[shipI][shipJ - tripleShipCount] = tripleShip;
                tripleShipIList.add(shipI);
                tripleShipJList.add(shipJ - tripleShipCount);

                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        tripleBorderShipIList.add(shipI + j);
                        tripleBorderShipJList.add(shipJ - tripleShipCount + i);
                    }
                }
                tripleShipCount++;
            }
        }

    }

    public static boolean checkIfEnemyTriplePresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < TripleShip.getEnemyTripleShipIList().size(); i++) {
            if (TripleShip.getEnemyTripleShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (TripleShip.getEnemyTripleShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfPlayerTriplePresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < TripleShip.getPlayerTripleShipIList().size(); i++) {
            if (TripleShip.getPlayerTripleShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (TripleShip.getPlayerTripleShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfEnemyTripleBordersPresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < TripleShip.getEnemyTripleBorderShipIList().size(); i++) {
            if (TripleShip.getEnemyTripleBorderShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (TripleShip.getEnemyTripleBorderShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfPlayerTripleBordersPresent(Integer numberI, Integer numberJ) {
        List<Integer> indexList = new ArrayList<>();

        for (int i = 0; i < TripleShip.getPlayerTripleBorderShipIList().size(); i++) {
            if (TripleShip.getPlayerTripleBorderShipIList().get(i).equals(numberI)) {
                indexList.add(i);
            }
        }
        for (Integer list : indexList) {
            if (TripleShip.getPlayerTripleBorderShipJList().get(list).equals(numberJ)) {
                return true;
            }
        }
        return false;
    }
}
