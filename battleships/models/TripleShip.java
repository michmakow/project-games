package Project_Games.battleships.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TripleShip {
    private final static String name = "T";
    private int flagCount = 3;
    private int drawDirection = new Random().nextInt(3);

    private static List<Integer> enemyTripleShipIList = new ArrayList();
    private static List<Integer> enemyTripleShipJList = new ArrayList();
    private static List<Integer> enemyTripleBorderShipIList = new ArrayList();
    private static List<Integer> enemyTripleBorderShipJList = new ArrayList();

    private static List<Integer> playerTripleShipIList = new ArrayList();
    private static List<Integer> playerTripleShipJList = new ArrayList();
    private static List<Integer> playerTripleBorderShipIList = new ArrayList();
    private static List<Integer> playerTripleBorderShipJList = new ArrayList();

    public int getFlagCount() {
        return flagCount;
    }

    public void setDrawDirection(int drawDirection) {
        this.drawDirection = drawDirection;
    }

    public static List<Integer> getEnemyTripleShipIList() {
        return enemyTripleShipIList;
    }

    public static List<Integer> getEnemyTripleShipJList() {
        return enemyTripleShipJList;
    }

    public static List<Integer> getEnemyTripleBorderShipIList() {
        return enemyTripleBorderShipIList;
    }

    public static List<Integer> getEnemyTripleBorderShipJList() {
        return enemyTripleBorderShipJList;
    }

    public static List<Integer> getPlayerTripleShipIList() {
        return playerTripleShipIList;
    }

    public static List<Integer> getPlayerTripleShipJList() {
        return playerTripleShipJList;
    }

    public static List<Integer> getPlayerTripleBorderShipIList() {
        return playerTripleBorderShipIList;
    }

    public static List<Integer> getPlayerTripleBorderShipJList() {
        return playerTripleBorderShipJList;
    }

    @Override
    public String toString() {
        return name + " ";
    }
}

