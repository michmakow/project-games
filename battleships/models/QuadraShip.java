package Project_Games.battleships.models;

import Project_Games.battleships.BattleshipsOperator;

import java.util.*;

public class QuadraShip {
    private final static String name = "Q";
    private int flagCount = 4;
    private int drawDirection = new Random().nextInt(3);

    private static List<Integer> enemyQuadraShipIList = new ArrayList();
    private static List<Integer> enemyQuadraShipJList = new ArrayList();
    private static List<Integer> enemyQuadraBorderShipIList = new ArrayList();
    private static List<Integer> enemyQuadraBorderShipJList = new ArrayList();

    private static List<Integer> playerQuadraShipIList = new ArrayList();
    private static List<Integer> playerQuadraShipJList = new ArrayList();
    private static List<Integer> playerQuadraBorderShipIList = new ArrayList();
    private static List<Integer> playerQuadraBorderShipJList = new ArrayList();

    public int getFlagCount() {
        return flagCount;
    }

    public void setDrawDirection(int drawDirection) {
        this.drawDirection = drawDirection;
    }

    public static List<Integer> getEnemyQuadraShipIList() {
        return enemyQuadraShipIList;
    }

    public static List<Integer> getEnemyQuadraShipJList() {
        return enemyQuadraShipJList;
    }

    public static List<Integer> getEnemyQuadraBorderShipIList() {
        return enemyQuadraBorderShipIList;
    }

    public static List<Integer> getEnemyQuadraBorderShipJList() {
        return enemyQuadraBorderShipJList;
    }

    public static List<Integer> getPlayerQuadraShipIList() {
        return playerQuadraShipIList;
    }

    public static List<Integer> getPlayerQuadraShipJList() {
        return playerQuadraShipJList;
    }

    public static List<Integer> getPlayerQuadraBorderShipIList() {
        return playerQuadraBorderShipIList;
    }

    public static List<Integer> getPlayerQuadraBorderShipJList() {
        return playerQuadraBorderShipJList;
    }

    @Override
    public String toString() {
        return name + " ";
    }
}
