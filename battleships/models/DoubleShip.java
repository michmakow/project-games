package Project_Games.battleships.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DoubleShip {
    private final static String name = "D";
    private int flagCount = 2;
    private int drawDirection = new Random().nextInt(3);

    private static List<Integer> enemyDoubleShipIList = new ArrayList();
    private static List<Integer> enemyDoubleShipJList = new ArrayList();
    private static List<Integer> enemyDoubleBorderShipIList = new ArrayList();
    private static List<Integer> enemyDoubleBorderShipJList = new ArrayList();

    private static List<Integer> playerDoubleShipIList = new ArrayList();
    private static List<Integer> playerDoubleShipJList = new ArrayList();
    private static List<Integer> playerDoubleBorderShipIList = new ArrayList();
    private static List<Integer> playerDoubleBorderShipJList = new ArrayList();

    public int getFlagCount() {
        return flagCount;
    }

    public void setDrawDirection(int drawDirection) {
        this.drawDirection = drawDirection;
    }

    public static List<Integer> getEnemyDoubleShipIList() {
        return enemyDoubleShipIList;
    }

    public static List<Integer> getEnemyDoubleShipJList() {
        return enemyDoubleShipJList;
    }

    public static List<Integer> getEnemyDoubleBorderShipIList() {
        return enemyDoubleBorderShipIList;
    }

    public static List<Integer> getEnemyDoubleBorderShipJList() {
        return enemyDoubleBorderShipJList;
    }

    public static List<Integer> getPlayerDoubleShipIList() {
        return playerDoubleShipIList;
    }

    public static List<Integer> getPlayerDoubleShipJList() {
        return playerDoubleShipJList;
    }

    public static List<Integer> getPlayerDoubleBorderShipIList() {
        return playerDoubleBorderShipIList;
    }

    public static List<Integer> getPlayerDoubleBorderShipJList() {
        return playerDoubleBorderShipJList;
    }

    @Override
    public String toString() {
        return name + " ";
    }
}
