package Project_Games.battleships.models;

import java.util.ArrayList;
import java.util.List;

public class SingleShip {

    private final static String name = "S";
    private int flagCount = 1;

    private static List<Integer> enemySingleShipIList = new ArrayList();
    private static List<Integer> enemySingleShipJList = new ArrayList();
    private static List<Integer> enemySingleBorderShipIList = new ArrayList();
    private static List<Integer> enemySingleBorderShipJList = new ArrayList();

    private static List<Integer> playerSingleShipIList = new ArrayList();
    private static List<Integer> playerSingleShipJList = new ArrayList();
    private static List<Integer> playerSingleBorderShipIList = new ArrayList();
    private static List<Integer> playerSingleBorderShipJList = new ArrayList();

    public static List<Integer> getEnemySingleShipIList() {
        return enemySingleShipIList;
    }

    public static void addEnemySingleShipIList(Integer shipI) {
        SingleShip.enemySingleShipIList.add(shipI);
    }

    public static List<Integer> getEnemySingleShipJList() {
        return enemySingleShipJList;
    }

    public static void addEnemySingleShipJList(Integer shipJ) {
        SingleShip.enemySingleShipJList.add(shipJ);
    }

    public static List<Integer> getEnemySingleBorderShipIList() {
        return enemySingleBorderShipIList;
    }

    public static void addEnemySingleBorderShipIList(Integer shipI) {
        SingleShip.enemySingleBorderShipIList.add(shipI);
    }

    public static List<Integer> getEnemySingleBorderShipJList() {
        return enemySingleBorderShipJList;
    }

    public static void addEnemySingleBorderShipJList(Integer shipJ) {
        SingleShip.enemySingleBorderShipJList.add(shipJ);
    }

    public static List<Integer> getPlayerSingleShipIList() {
        return playerSingleShipIList;
    }

    public static void addPlayerSingleShipIList(Integer shipI) {
        SingleShip.playerSingleShipIList.add(shipI);
    }

    public static List<Integer> getPlayerSingleShipJList() {
        return playerSingleShipJList;
    }

    public static void addPlayerSingleShipJList(Integer shipJ) {
        SingleShip.playerSingleShipJList.add(shipJ);
    }

    public static List<Integer> getPlayerSingleBorderShipIList() {
        return playerSingleBorderShipIList;
    }

    public static void addPlayerSingleBorderShipIList(Integer shipI) {
        SingleShip.playerSingleBorderShipIList.add(shipI);
    }

    public static List<Integer> getPlayerSingleBorderShipJList() {
        return playerSingleBorderShipJList;
    }

    public static void addPlayerSingleBorderShipJList(Integer shipJ) {
        SingleShip.playerSingleBorderShipJList.add(shipJ);
    }

    @Override
    public String toString() {
        return name + " ";
    }
}
