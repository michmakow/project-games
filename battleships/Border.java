package Project_Games.battleships;

public class Border {

    private final Object[][] leftAndRighBorder(Object[][] map) {

        for (int j = 1; j <= 9; j++) {
            map[11 - j][0] = (j + " ");
            map[11 - j][11] = String.valueOf(j);
        }

        map[1][0] = "10";
        map[1][11] = "10";
        return map;
    }

    private final Object[][] topAndBottomBorder(Object[][] map) {
        int k = 1;
        for (char ch = 'A'; ch <= 'J'; ch++) {
            map[11][k] = String.valueOf(ch + " ");
            map[0][k] = String.valueOf(ch + " ");
            k++;
        }
        return map;
    }

    public final Object[][] mapFormatter(Object[][] map) {

        for (int i = 0; i <= 11; i++) {
            for (int j = 0; j <= 11; j++) {
                map[i][j] = "  ";
            }
        }

        leftAndRighBorder(map);
        topAndBottomBorder(map);
        return map;
    }

    public void mapDisplay(Object[][] map) {

        for (int i = 0; i <= 11; i++) {

            for (int j = 0; j <= 11; j++) {
                System.out.printf("%s", map[i][j]);
            }
            System.out.println();
        }
    }
}
