package Project_Games.battleships.views;

import Project_Games.sharedClasses.ProperPrinter;

public class DoubleShipView {
    private static final int PRINTER_WIDTH = 80;
    private static ProperPrinter pp = new ProperPrinter(PRINTER_WIDTH);

    public static void placeYourTwoMastedShip() {
        pp.print("Place your two-masted ship.");
    }

    public static void wrongInput() {
        pp.print("Wrong input. Please try again.");
    }

    public static void chooseDiffDirOrCoordi() {
        pp.print("Choose different direction or coordinates.");
    }

    public static void youHitTwoMasted() {
        pp.print("Good job! You hit a two-masted ship");
    }

    public static void enemyHitTwoMasted() {
        pp.print("Enemy hit your two-masted ship!!");
    }
}

