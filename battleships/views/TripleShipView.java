package Project_Games.battleships.views;

import Project_Games.sharedClasses.ProperPrinter;

public class TripleShipView {
    private static final int PRINTER_WIDTH = 80;
    private static ProperPrinter pp = new ProperPrinter(PRINTER_WIDTH);

    public static void placeYourThreeMastedShip() {
        pp.print("Place your three-masted ship.");
    }

    public static void wrongInput() {
        pp.print("Wrong input. Please try again.");
    }

    public static void chooseDiffDirOrCoordi() {
        pp.print("Choose different direction or coordinates.");
    }

    public static void youHitThreeMasted() {
        pp.print("Good job! You hit a three-masted ship");
    }

    public static void enemyHitThreeMasted() {
        pp.print("Enemy hit your three-masted ship!!");
    }
}
