package Project_Games.battleships.views;

import Project_Games.sharedClasses.ProperPrinter;

public class SingleShipView {
    private static final int PRINTER_WIDTH = 80;
    private static ProperPrinter pp = new ProperPrinter(PRINTER_WIDTH);

    public static void placeYourOneMastedShip() {
        pp.print("Place your one-masted ship.");
    }

    public static void wrongInput() {
        pp.print("Wrong input. Please try again.");
    }

    public static void chooseDiffDirOrCoordi() {
        pp.print("Choose different direction or coordinates.");
    }

    public static void youHitOneMasted() {
        pp.print("Good job! You hit a one-masted ship");
    }

    public static void enemyHitOneMasted() {
        pp.print("Enemy hit your one-masted ship!!");
    }
}
