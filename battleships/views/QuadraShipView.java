package Project_Games.battleships.views;

import Project_Games.sharedClasses.ProperPrinter;

public class QuadraShipView {
    private static final int PRINTER_WIDTH = 80;
    private static ProperPrinter pp = new ProperPrinter(PRINTER_WIDTH);

    public static void placeYourFourMastedShip() {
        pp.print("Place your four-masted ship.");
    }

    public static void wrongInput() {
        pp.print("Wrong input. Please try again.");
    }

    public static void chooseDiffDirOrCoordi() {
        pp.print("Choose different direction or coordinates.");
    }

    public static void youHitFourMasted(){
        pp.print("Good job! You hit a four-masted ship");
    }

    public static void enemyHitFourMasted() {
        pp.print("Enemy hit your four-masted ship!!");
    }
}
