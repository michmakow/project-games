package Project_Games.sharedClasses;

import Project_Games.battleships.Battleships;
import Project_Games.guessNumber.GuessNumber;
import Project_Games.hangMan.HangMan;
import Project_Games.millionaire.WhoWantsToBeMillionaire;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class GameOperator implements Playable{

    List<Playable> games = new ArrayList<>();
    public static final int PRINTER_WIDTH = 40;
    ProperPrinter pp = new ProperPrinter(PRINTER_WIDTH);


    public GameOperator() {
        games.add(new GuessNumber());
        games.add(new HangMan());
        games.add(new WhoWantsToBeMillionaire());
        games.add(new Battleships());

    }

    public void start(){

        pp.delim();
        for(Playable playable: games){
            pp.print(playable.getDescription()+ " "+playable.getNumber());
        }
        pp.delim();

        Scanner scanner = new Scanner(System.in);
        int numberOfGame = scanner.nextInt();

        getGame(numberOfGame).start();
    }

    private Playable getGame(int numberOfGame) {
        Optional<Playable> optional = games.stream().filter(x->x.getNumber()==numberOfGame)
                .findFirst();

        if(optional.isPresent()) return optional.get();
        return games.get(0);
    }

    @Override
    public int getNumber() {
        return 0;
    }

    @Override
    public String getDescription() {
        return null;
    }
}
