package Project_Games.sharedClasses;

public interface Playable {
    void start();
    int getNumber();
    String getDescription();
}
